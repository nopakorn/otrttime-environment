using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using otrttime_environment.Model;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace otrttime_environment.Controllers
{
    [Route("api/[controller]")]
    public class SampleDataController : Controller
    {
        private readonly OTRTTime_TestContext _context;

        public SampleDataController(OTRTTime_TestContext context)
        {
            _context = context;
        }
        private static string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private int GetUserIdFromCredentials(LoginViewModel loginViewModel)
        {
            using (var ctx = _context)
            {
                var userId = ctx.User.Where(u => u.Email.Equals(loginViewModel.Email) && u.Password.Equals(loginViewModel.Password))
                .Select(x => x.Id);
                if(userId != null)
                {
                    return Convert.ToInt32(userId);
                }
            }
            return -1;
        }

        [Authorize]
        [HttpGet("[action]")]
        public User GetUser()
        {
            using(var ctx = _context)
            {
                var user = ctx.User.FirstOrDefault();
                return user;
            }
        }

        [HttpGet("[action]")]
        public IEnumerable<WeatherForecast> WeatherForecasts()
        {
            var rng = new Random();
            return Enumerable.Range(1, 5).Select(index => new WeatherForecast
            {
                DateFormatted = DateTime.Now.AddDays(index).ToString("d"),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            });
        }

        public class WeatherForecast
        {
            public string DateFormatted { get; set; }
            public int TemperatureC { get; set; }
            public string Summary { get; set; }

            public int TemperatureF
            {
                get
                {
                    return 32 + (int)(TemperatureC / 0.5556);
                }
            }
        }
    }
}
